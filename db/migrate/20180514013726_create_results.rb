class CreateResults < ActiveRecord::Migration[5.1]
  def change
    create_table :results do |t|
      t.integer :number1
      t.integer :number2

      t.timestamps
    end
  end
end
