class PagesController < ApplicationController
  def envio
  end

  def save_result
    @result = Result.create!(number1: params[:a], number2: params[:b])
    redirect_to pages_recibo_path(id: @result.id)
  end

  def recibo
    # a = params[:a].to_i
    # b = params[:b].to_i
    # @suma = a + b

    @result = Result.find(params[:id])
    @suma = @result.number1 + @result.number2
  end
end
